using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float Jumpforce, speed;
    private Rigidbody2D Rigibody2d;
    private float Horizontal;
    private bool movement = true;
    private bool Ground;
    private Animator Animator;
    // Start is called before the first frame update
    void Start()
    {
        Animator = GetComponent<Animator>();
        Rigibody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Horizontal = Input.GetAxisRaw("Horizontal");
        if (!movement) Horizontal = 0;
        if (Horizontal < 0.0f) transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        else if (Horizontal > 0.0f) transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

        Animator.SetBool("Runing", Horizontal != 0.0f);

        Debug.DrawRay(transform.position, Vector3.down * 1.1f, Color.blue);
        if (Physics2D.Raycast(transform.position, Vector3.down, 1.1f))
        {
            Ground = true;
            Animator.SetBool("jump2", false);
            Animator.SetBool("jump", false);
            
        }
        else
        {
            Animator.SetBool("jump2", true);
            Animator.SetBool("jump", true);
            Ground = false;
        }
        if (Input.GetKeyDown(KeyCode.W) && Ground)
        {
            Jump();
        }
        if (Input.GetKeyDown(KeyCode.F) && Ground)
        {
            Animator.SetBool("Attack", true);
        }
        if (Input.GetKeyUp(KeyCode.F) && Ground)
        {
            Animator.SetBool("Attack", false);
        }
        if (Input.GetKeyDown(KeyCode.E) && Ground)
        {
            Animator.SetBool("dano", true);
        }
        if (Input.GetKeyUp(KeyCode.E) && Ground)
        {
            Animator.SetBool("dano", false);
        }
    }
    void Jump()
    {
        Rigibody2d.AddForce(Vector2.up * Jumpforce, ForceMode2D.Impulse);
    }
    private void FixedUpdate()
    {
        Rigibody2d.velocity = new Vector2(Horizontal* speed, Rigibody2d.velocity.y);
    }
}
